#include <mpi.h>
#include <stdio.h>
#define ROOT 0
#define MSG_TAG 100

int main(int argc, char **argv)
{
	int tid,size;
	MPI_Status status;

	MPI_Init(&argc, &argv); //Musi być w każdym programie na początku

	printf("Checking!\n");
	MPI_Comm_size( MPI_COMM_WORLD, &size );
	MPI_Comm_rank( MPI_COMM_WORLD, &tid );
	printf("My id is %d from %d\n",tid, size);

	int msg[2];

	if ( tid == ROOT)
	{
        int i = 0;
        MPI_Send(&i, 1, MPI_INT, 1 , MSG_TAG, MPI_COMM_WORLD );
        printf(" Wysłałem %d do %d\n", i, 1);
		MPI_Recv(&i, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
		printf(" Otrzymalem %d od %d\n", i, status.MPI_SOURCE);
		
	}
	else
	{
		int i = 0;
        MPI_Recv(&i, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        printf(" Otrzymalem %d od %d\n", i, status.MPI_SOURCE);
        i++;
        MPI_Send(&i, 1, MPI_INT, (i + 1) % size, MSG_TAG, MPI_COMM_WORLD );
		printf(" Wyslalem %d do %d\n", i, (i + 1) % size);
	}

	MPI_Finalize(); // Musi być w każdym programie na końcu
}
