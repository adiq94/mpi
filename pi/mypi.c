#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>
#include <math.h>

#define ROOT 0
#define MSG_TAG 100
#define WORKER_NUM 4

int main(int argc,char **argv)
{
    int size,tid;
    int R = 999; //1

    MPI_Init(&argc, &argv); 

    MPI_Comm_size( MPI_COMM_WORLD, &size );
    MPI_Comm_rank( MPI_COMM_WORLD, &tid );

    srand( tid );

    int res;

    if ( tid == 0 ) {
	MPI_Status status;
    long double pi = 0;
    int inside = 0;
    int total = 0;
	// pewnie jakiś for tutaj
    for(int i = 0; i < 100000 * WORKER_NUM; i++){
        MPI_Recv( &res, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        total++;
        inside += res;
    }
    pi = 4.0 * ((double)inside / (double)total);
    printf("Inside: %d \n", inside);
    printf("Total: %d \n", total);
    printf("Przybliżenie pi po zebraniu danych od %d procesów wynosi %llf\n", WORKER_NUM, pi);
    } else {
        for(int i = 0; i < 100000; i++){
            int x = rand() % R;
            int y = rand() % R;
            // printf("x:%d,y:%d, R:%d", x, y, R);
            if(x*x + y*y < R*R){
                res = 1;
            } else{
                res = 0;
            }
            MPI_Send( &res, 1, MPI_INT, ROOT, MSG_TAG, MPI_COMM_WORLD );
        }	
    }

    MPI_Finalize();
}
