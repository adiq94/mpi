#ifndef LIBFUNH
#define LIBFUNH

/* koncepcyjnie będzie w przyszłości współdzielone między kilkoma programami */
#include "global.h"

/* odkomentować, jeżeli się chce DEBUGI */
//#define DEBUG 
/* boolean */
#define TRUE 1
#define FALSE 0

#define ROOT 0

/*
    LISTA FUNKCJI
    -------------

    debug - printf który wyświetla się po zdefiniowaniu DEBUG
    inicjuj - inicjalizuje MPI, wątek "opóźniający" oraz wątek symulujący aplikację
    finalizuj - kończy MPI oraz wątki
    println - makro: taki printf, ale z kolorkami, automaycznym \n oraz wypisywaniem ranka
    check_thread_support - wypisuje co oznacza "provided" dla MPI_Init_thread, kończy gdy provided=brak wątków
    push_pkt - wrzuca pakiet na stos stack
    pop_pkt - zdejmuje pakiet ze stosu stack
    addHandler - dodaje handler pakietu o danym typie
    sendPacket - wysyła pakiet do danego procesu z podanym tagiem
    
    dokładniejszy opis poniżej

    program powinien wyglądać tak:
        
    int main(int argc, char **argv)
    {
        inicjuj(&argc, &argv);
        main_loop();
        finalizuj();
    }

    aby dodać handlery do typów wiadomości: użyj addHandler( typ, funkcja ). 
    Zobacz app.h

    Właśne definicje wątków: wystarczy podmienić app.c (zmienić startFunc delayComFunc)
    Parametry: zmieniamy w global.h
*/
    

/* macro debug - działa jak printf, kiedy zdefiniowano
   DEBUG, kiedy DEBUG niezdefiniowane działa jak instrukcja pusta 
   
   używa się dokładnie jak printfa, tyle, że dodaje kolorków i automatycznie
   wyświetla rank

   w związku z tym, zmienna "rank" musi istnieć.
*/
#ifdef DEBUG
#define debug(...) printf("%c[%d;%dm [%d]: " FORMAT "%c[%d;%dm\n",  27, (1+(rank/7))%2, 31+(6+rank)%7, rank, ##__VA_ARGS__, 27,0,37);

#else
#define debug(...) ;
#endif

#define P_WHITE printf("%c[%d;%dm",27,1,37);
#define P_BLACK printf("%c[%d;%dm",27,1,30);
#define P_RED printf("%c[%d;%dm",27,1,31);
#define P_GREEN printf("%c[%d;%dm",27,1,33);
#define P_BLUE printf("%c[%d;%dm",27,1,34);
#define P_MAGENTA printf("%c[%d;%dm",27,1,35);
#define P_CYAN printf("%c[%d;%d;%dm",27,1,36);
#define P_SET(X) printf("%c[%d;%dm",27,1,31+(6+X)%7);
#define P_CLR printf("%c[%d;%dm",27,0,37);

#define println(FORMAT, ...) printf("%c[%d;%dm [%d]: " FORMAT "%c[%d;%dm\n",  27, (1+(rank/7))%2, 31+(6+rank)%7, rank, ##__VA_ARGS__, 27,0,37);

/* Jeden argument to "provided" (trzeci argument MPI_Init_thread). 
   Funkcja wypisuje po ludzku, co ten provided oznacza.
   Przy braku wsparcia dla wątków, kończy program
*/
void check_thread_support(int);

/* end == TRUE oznacza wyjście z main_loop */
extern volatile char end;
/* ile procesów odpalono, jaki jest mój identyfikator "rank" */
extern int size,rank;
/* synchronizacja dostępu do funkcji MPI */
extern pthread_mutex_t mut;
/* wątek symulujący proces aplikacyjny, wątek symulujący opóźnienia komunikacyjne */
extern pthread_t threadA, threadB;


/* wrzuca pakiet (pierwszy argument) oraz status (drugi argument) jako element stosu 
   stos w zmiennej stack
*/
void push_pkt( packet_t *, MPI_Status);
/* zdejmuje ze stosu pakiet i zwraca go, status związany z tym pakietem zwraca w 
  pierwszym argumencie (który trzeba przekazać przez wskaźnik

  przykład użycia: packet_t *pakiet = pop_pkt(&status); //MPI_Status status
*/
packet_t *pop_pkt(MPI_Status *);

/*
    Dodaje handler.
    Przykład: 
    void funkcja(packet_t *pakiet);
    i potem w funkcji main PRZED main_loop:
    addHandler( MOJ_TYP, funkcja)

    dwa handlery już są (dla FINISH i APP_MSG) z ciałami w app.c
*/
void addHandler(int, void (*)(packet_t *));

/* funkcja wysyła podany pakiet do podanego procesu z podanym tagiem*/
void sendPacket(packet_t *, int destination, int tag);

/* argumenty to &argc i &argv
  inicjalizuje MPI, zmienne size i rank, odpala wątki aplikacyjny i komunikacyjny
*/
void inicjuj( int *, char ***);

/* czeka na zakończenie wątków, sprząta, finalizuje MPI */
void finalizuj();

/* czeka na nadejście wiadomości i wrzuca je na stos do późniejszej obsługi przez
   wątek komunikacyjny. Kończy się, gdy end == TRUE
*/
void main_loop();


/* typy wiadomości */
#define FINISH 1
#define APP_MSG 2
#define GIVE_YOUR_STATE 3
#define MY_STATE_IS 4

/* definiowany typ danych */
extern MPI_Datatype MPI_PAKIET_T;

typedef struct {
    int tag;
    void (*handler)(packet_t *);
} handler_t;

extern int lastHandler;
extern handler_t handlers[MAXHANDLERS];
#endif
