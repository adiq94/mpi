#ifndef GLOBALH
#define GLOBALH
#define _GNU_SOURCE
#include <mpi.h>
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <unistd.h>
#include <string.h>

/* parametry konkretnego programu */

#define PROB_OF_SENDING 25
#define PROB_OF_PASSIVE 5
#define PROB_OF_SENDING_DECREASE 1
#define PROB_SENDING_LOWER_LIMIT 1
#define PROB_OF_PASSIVE_INCREASE 1

#define MAXHANDLERS 50

/* to może przeniesiemy do global... */
typedef struct {
    int appdata;
    int inne_pole; /* można zmienić nazwę na bardziej pasującą */
    int src; /* pole nie przesyłane, ale ustawiane w main_loop */
} packet_t;

/* w przypadku zmiany struktury, należy także zmienić inicjuj w libfun.c 
   w szczególności wartość zmiennych nitems itd odpowiednio wg wzorca */

#endif
