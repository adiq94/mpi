#ifndef APP_H
#define APP_H

#include "global.h"
#include "libfun.h"
/* 
    LISTA FUNKCJI
    -------------

    comDelayFunc - pętla wątku symulującego opóźnienia komunikacyjne
    startFunc - pętla wątku symulującego proces aplikacyjny

    wątek komunikacyjny wywołuje zdefiniowane handlery.
    Maksymalnie MAXHANDLERS (global.h) handlerów, z czego
    dwa już są zdefiniowane (dla FINISH i MSG_APP).
    FINISH kończy działanie aplikacji.
    Wątek ten symuluje opóźnienia.

    wątek aplikacyjny symuluje działanie aplikacji. Zmienia stan
    z passive na active, losowo czasami wysyła wiadomości do losowego procesu.
    W global.h są zmienne określające prawdopodobieństwa, dla których proces
    wysyła wiadomość, czy prawdopodobieństwo to spada z czasem; oraz 
    prawdopodobieństwo, czy proces staje się pasywny (i czy spada ono z czasem(
    
    (c) wiadomo kto
*/

/* symulowanie procesu aplikacyjnego */
/* koncepcyjnie, może będzie współdzielone między wieloma programami */

/* argument musi być, bo wymaga tego pthreads. Wątek komunikacyjny */
void *comDelayFunc(void *arg);
/* argument musi być, bo wymaga tego pthreads. Wątek aplikacyjny, wysyła losowo wiadomości */
void *startFunc(void *ptr);
void *monitorFunc(void *ptr);

extern char passive;
extern int state;
extern pthread_mutex_t state_mut;

#endif
