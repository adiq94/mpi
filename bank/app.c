#include "global.h"
#include "libfun.h"
#include "app.h"

#include <pthread.h>



/* stan procesu */
char passive = FALSE;
int state = 10000;

int prob_of_passive=PROB_OF_PASSIVE;
int prob_of_sending=PROB_OF_SENDING;


pthread_mutex_t state_mut = PTHREAD_MUTEX_INITIALIZER;
void finishHandler( packet_t *pakiet)
{
    println("Otrzymałem FINISH" );
    passive = TRUE;
    end = TRUE; 
}

void giveHandler( packet_t *pakiet)
{
    println("dostałem GIVE STATE\n");
   
    packet_t tmp;
    tmp.appdata = state; 
    sendPacket(&tmp, ROOT, MY_STATE_IS);
}

void appMsgHandler( packet_t *pakiet)
{
    passive = FALSE;
    println("dostałem %d od %d\n", pakiet->appdata, pakiet->src);
    pthread_mutex_lock(&state_mut);
	state+=pakiet->appdata;
    println("Stan obecny: %d\n", state);
    pthread_mutex_unlock(&state_mut);
}

int suma = 0;
int answers;
void myStateHandler(packet_t *pakiet)
{

    suma += pakiet->appdata;
    answers++;

    /* W ogólności wynik będzie błędny (część kasy jest w kanałach)  */
    println(" Suma %d \n", suma);

    if (answers == size ) {
	println("Ostateczna suma %d \n", suma);
        int i;
        packet_t data;
	for (i=0;i<size;i++)  
            sendPacket( &data, i, FINISH);
	println("MONITOR KONIEC \n");
    }
}

/* symulowanie opóźnień komunikacyjnej */
void *comDelayFunc(void *arg)
{
    int percent;
    packet_t *ptr=NULL;
    MPI_Status status;
    addHandler(FINISH, finishHandler );
    addHandler(APP_MSG, appMsgHandler );
    addHandler( GIVE_YOUR_STATE, giveHandler);
    addHandler( MY_STATE_IS, myStateHandler);

    while (!end) {

        while ((ptr=pop_pkt(&status)) == NULL ) {
            pthread_yield(); 
        }
        
        ptr->src = status.MPI_SOURCE;
	percent = rand()%3 + 1;
        struct timespec t = { percent, 0 };
        struct timespec rem = { 1, 0 };
        nanosleep(&t,&rem);
    
        int i;
        /* TUTAJ DOPIERO OBSŁUGA ZEGARA LAMPORTA */

	for (i=0;i<lastHandler;i++) {
	    if (handlers[i].tag == status.MPI_TAG) {
		handlers[i].handler( ptr ); // Albo: Jako argument podać aktualny zegar Lamporta!
	    }
        /* Albo: dopiero tutaj zwolnić muteks związany z zegarem lamporta */
        }
        free( ptr );
    }

    return (void *)0;
}

/* Kod funkcji wykonywanej przez wątek */
void *startFunc(void *ptr)
{
    /* wątek się kończy, gdy funkcja się kończy */
    int percent,dst;
    packet_t pakiet;
    while ( !end ) {
	percent = rand()%2 + 1;
        struct timespec t = { percent, 0 };
        struct timespec rem = { 1, 0 };
        nanosleep(&t,&rem);

	percent = rand()%100;
	if ((percent < prob_of_sending ) && (state >0)) {
	    do {
		dst = rand()%(size);
		//if (dst==size) MPI_Abort(MPI_COMM_WORLD,1); // strzeżonego :D
	    } while (dst==rank);

	    percent = rand()%state;
	    pakiet.appdata = percent;
	    pthread_mutex_lock(&state_mut);
            state-=percent;
	    pthread_mutex_unlock(&state_mut);
	    //pthread_mutex_lock(&mut);
	    sendPacket(&pakiet, dst, APP_MSG);
	    //pthread_mutex_unlock(&mut);
	    if (prob_of_sending > PROB_SENDING_LOWER_LIMIT)
		prob_of_sending -= PROB_OF_SENDING_DECREASE;

	    debug("[%d] wysłałem %d do %d\n", rank, pakiet.appdata, dst);
        }  
    }

    return (void *)0;
}

void *monitorFunc(void *ptr)
{
    packet_t data;
	/* MONITOR; Jego zadaniem ma być wykrycie, ile kasy jest w systemie */

	// 10 sekund, coby procesy zdążyły namieszać w stanie globalnym
    sleep(10);
	// TUTAJ WYKRYWANIE STANu        
    int i;
    println("MONITOR START \n");
    for (i=0;i<size;i++)  {
	//pthread_mutex_lock(&mut);
        sendPacket( &data, i, GIVE_YOUR_STATE);
	//pthread_mutex_unlock(&mut);
    }

    return 0;
}

