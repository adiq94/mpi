#include <mpi.h>
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>

/* wątki */
#include <pthread.h>

#include "libfun.h"
#include "app.h"

int lastHandler;
handler_t handlers[MAXHANDLERS];

volatile char end = FALSE;
int size,rank;
MPI_Datatype MPI_PAKIET_T;
pthread_t threadA, threadB, threadC;

pthread_mutex_t mut = PTHREAD_MUTEX_INITIALIZER;

typedef struct stack_s {
    packet_t *pakiet;
    struct stack_s *prev;
    MPI_Status status;
} stack_t;

stack_t *stack;

void check_thread_support(int provided)
{
    printf("THREAD SUPPORT: %d\n", provided);
    switch (provided) {
        case MPI_THREAD_SINGLE: 
            printf("Brak wsparcia dla wątków, kończę\n");
            /* Nie ma co, trzeba wychodzić */
	    fprintf(stderr, "Brak wystarczającego wsparcia dla wątków - wychodzę!\n");
	    MPI_Finalize();
	    exit(-1);
	    break;
        case MPI_THREAD_FUNNELED: 
            printf("tylko te wątki, ktore wykonaly mpi_init_thread mogą wykonać wołania do biblioteki mpi\n");
	    break;
        case MPI_THREAD_SERIALIZED: 
            /* Potrzebne zamki wokół wywołań biblioteki MPI */
            printf("tylko jeden watek naraz może wykonać wołania do biblioteki MPI\n");
	    break;
        case MPI_THREAD_MULTIPLE: printf("Pełne wsparcie dla wątków\n");
	    break;
        default: printf("Nikt nic nie wie\n");
    }
}

pthread_mutex_t stack_mtx = PTHREAD_MUTEX_INITIALIZER;

void push_pkt( packet_t *pakiet, MPI_Status status)
{
    stack_t *tmp = malloc(sizeof(stack_t));
    tmp->pakiet = pakiet;
    tmp->status = status;

    pthread_mutex_lock(&stack_mtx);
    tmp->prev = stack; 
    stack = tmp;
    pthread_mutex_unlock(&stack_mtx);
}

packet_t *pop_pkt(MPI_Status *status) 
{
    pthread_mutex_lock(&stack_mtx);
    if (stack == NULL ) {
	pthread_mutex_unlock(&stack_mtx);
        return NULL;
    } else {
        packet_t *tmp = stack->pakiet;
        stack_t *prev = stack->prev;
        *status= stack->status;
        free(stack);
        stack = prev;
	pthread_mutex_unlock(&stack_mtx);
        return tmp; 
    }
}


void inicjuj(int *argc, char ***argv)
{
    int provided;
    MPI_Init_thread(argc, argv,MPI_THREAD_MULTIPLE, &provided);
    check_thread_support(provided);


    /* Stworzenie typu */
    /* Poniższe (aż do MPI_Type_commit) potrzebne tylko, jeżeli
       brzydzimy się czymś w rodzaju MPI_Send(&typ, sizeof(pakiet_t), MPI_BYTE....
    */
    /* sklejone z stackoverflow */
    const int nitems=2;
    int       blocklengths[2] = {1,1};
    MPI_Datatype typy[2] = {MPI_INT, MPI_INT};
    MPI_Aint     offsets[2];

    offsets[0] = offsetof(packet_t, appdata);
    offsets[1] = offsetof(packet_t, inne_pole);

    MPI_Type_create_struct(nitems, blocklengths, offsets, typy, &MPI_PAKIET_T);
    MPI_Type_commit(&MPI_PAKIET_T);


    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    srand(rank);

    pthread_create( &threadA, NULL, startFunc, 0);
    pthread_create( &threadB, NULL, comDelayFunc, 0);
    if (rank==0) {
	pthread_create( &threadC, NULL, monitorFunc, 0);
    } 
}

void finalizuj()
{
    pthread_mutex_destroy( &mut);
    /* Czekamy, aż wątek potomny się zakończy */
    println("czekam na wątek \"aplikacyjny\"\n" );
    pthread_join(threadA,NULL);
    MPI_Type_free(&MPI_PAKIET_T);
    MPI_Finalize();
}

void main_loop()
{
    MPI_Status status;
    int is_message = FALSE;
    packet_t pakiet;
    /* Obrazuje pętlę odbierającą pakiety o różnych typach */
    while ( !end ) {
	debug("[%d] czeka na recv\n", rank);
        MPI_Recv( &pakiet, 1, MPI_PAKIET_T, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

        if (status.MPI_TAG == FINISH) end = TRUE;
        packet_t *pkt= malloc(sizeof(packet_t));
        memcpy(pkt,(const char *)&pakiet, sizeof(packet_t));
        push_pkt(pkt, status);
    }
}

/* Zero odporności na błędy, ale chyba nikt nie zrobi więcej niż 50 typów wiadomości? */
void addHandler(int tag, void (*handler)(packet_t *)) {
    handlers[lastHandler].tag =tag;
    handlers[lastHandler].handler =handler;
    lastHandler++;
    return;
}

void sendPacket(packet_t *pkt, int destination, int tag)
{
    MPI_Send( pkt, 1, MPI_PAKIET_T, destination, tag, MPI_COMM_WORLD);
}
