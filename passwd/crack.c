#include <mpi.h>
#define _XOPEN_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

/* 4 literowe
aa5UYq6trT5u.
bahAZ9Hk7SCf6
ddoo3WocSpthU
jkmD2RlhoMyuA
zzm4NUIIb7VIk
kkv864igyJC9o

5 literowe
aaSPfLTmjh3fU

6 literowe
aaLTdQr7DyHuU 
*/

char *stro="aaSPfLTmjh3fU";


int main(int argc, char **argv)
{
    MPI_Init(&argc, &argv);

    int size,rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    
    char cmp[6]={0};

    char salt[3]={0};
    salt[0]=stro[0];
    salt[1]=stro[1];

    char characters[27]={0};
    for (int i = 97; i <= 122; i++) {
        int index = i - 97;
        characters[index] = (char) i;
    }
    //printf("%s\n", characters);
    int i = rank;
    for(int j = 0; j < size; j++){
        for(int k = 0; k < size; k++){
            for(int l = 0; l < size; l++){
                for(int m = 0; m < size; m++){
                    cmp[0] = characters[i];
                    cmp[1] = characters[j];
                    cmp[2] = characters[k];
                    cmp[3] = characters[l];
                    cmp[4] = characters[m];
                    cmp[5] = '\0';
                    //printf("%s\n", cmp);
                    char *x=crypt(cmp, salt);
                    if ((strcmp(x,stro))==0)
                    {
                        /* w docelowej wersji przeslac odgadnięte hasło masterowi */
                        printf("Udalo sie: %s %s %s\n", cmp, x, stro);
                        MPI_Finalize();
                        exit(0);
                    }
                }
            }   
        }
    }

    MPI_Finalize();
}
