#include <mpi.h>
#include "lib.h"

#define TRANSFER_SET 0
#define TRANSFER_SET_SIZE 1
#define END_TRANSFER_SET 2
#define SEARCH_MODE 3

#define SEARCH_ALL_ELEMENTS 0
#define SEARCH_AT_LEAST_ONE_ELEMENT 1
#define SEARCH_NO_ELEMENTS 2
#define SEARCH_QUIT 3
#define SEARCH_TRANSFER_SET 4

#define ROOT 0

int main(int argc,char **argv)
{
    MPI_Init(&argc, &argv);

    int size;
    MPI_Status status;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    srand(ROOT);

    if(rank == ROOT){
        //generate array of sorted sets
        startTime();
        int **array = generateArrayOfSets();
        printf("ROOT: Minimum set size:%d, Maximum set size:%d\n", getMinSize(array), getMaxSize(array));
        printf("#%d: GENERATE ARRAY OF SORTED SETS %ld ms\n", rank, stopTime());
        //distribute sets
        startTime();
        for(int i = 0; i < ARRAY_SIZE; i++){
            int dest = (i % (size -1)) + 1;
            int dummy = SET_SIZE_MAX;
            MPI_Send(&dummy, 1, MPI_INT, dest, TRANSFER_SET_SIZE, MPI_COMM_WORLD );
            MPI_Send(array[i], SET_SIZE_MAX, MPI_INT, dest, TRANSFER_SET, MPI_COMM_WORLD );
        }
        printf("#%d: DISTRIBUTE SETS %ld ms\n", rank, stopTime());
        //end transfer
        for(int i = 0; i < size; i++){
            int dummy = 0;
            MPI_Send(&dummy, 1, MPI_INT, i, END_TRANSFER_SET, MPI_COMM_WORLD );
        }
        destroy2DArray(array);
        //generate searched set
        int searchedSize = 5;
        int *searchedSet = generateSet(searchedSize);
        printf("ROOT: Generated search set:\n");
        for(int i = 0; i < searchedSize; i++){
            printf("%d ", searchedSet[i]);
        }
        printf("\n");
        //send searched set to all
        for(int i = 1; i < size; i++){
            int dummy = SEARCH_TRANSFER_SET;
            MPI_Send(&dummy, 1, MPI_INT, i, SEARCH_MODE, MPI_COMM_WORLD );
            MPI_Send(&searchedSize, 1, MPI_INT, i, TRANSFER_SET_SIZE, MPI_COMM_WORLD );
            MPI_Send(searchedSet, searchedSize, MPI_INT, i, TRANSFER_SET, MPI_COMM_WORLD );
        }
        for(int i = 1; i < size; i++){
            int dummy = SEARCH_ALL_ELEMENTS;
            MPI_Send(&dummy, 1, MPI_INT, i, SEARCH_MODE, MPI_COMM_WORLD );
        }
        for(int i = 1; i < size; i++){
            int dummy = SEARCH_AT_LEAST_ONE_ELEMENT;
            MPI_Send(&dummy, 1, MPI_INT, i, SEARCH_MODE, MPI_COMM_WORLD );
        }
        for(int i = 1; i < size; i++){
            int dummy = SEARCH_NO_ELEMENTS;
            MPI_Send(&dummy, 1, MPI_INT, i, SEARCH_MODE, MPI_COMM_WORLD );
        }
        for(int i = 1; i < size; i++){
            int dummy = SEARCH_QUIT;
            MPI_Send(&dummy, 1, MPI_INT, i, SEARCH_MODE, MPI_COMM_WORLD );
        }
    } else{
        int **array = create2DArray(SET_SIZE_MAX, ARRAY_SIZE);
        int *searchedSet = createArray(SET_SIZE_MAX);
        int set_size = 0;
        int i = 0;
        while(1){
            MPI_Recv(&set_size, 1, MPI_INT, ROOT, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
            
            if (status.MPI_TAG==TRANSFER_SET_SIZE){
                MPI_Recv(array[i], set_size, MPI_INT, ROOT, TRANSFER_SET, MPI_COMM_WORLD, &status);
                i++;
            } else if(status.MPI_TAG==END_TRANSFER_SET){
                printf("#%d: Data sets size:%d\n", rank, i);
                break;
            }
        }
        int search_option = 0;
        
        do {
            MPI_Recv(&search_option, 1, MPI_INT, ROOT, SEARCH_MODE, MPI_COMM_WORLD, &status);
            switch(search_option){
                case SEARCH_ALL_ELEMENTS:
                    startTime();
                    searchAllElements(array, i, searchedSet, set_size, printSearchAllElements);
                    printf("#%d: SEARCH_ALL_ELEMENTS %ld ms\n", rank, stopTime());
                    break;
                case SEARCH_AT_LEAST_ONE_ELEMENT:
                    startTime();
                    searchAtLeastOneElement(array, i, searchedSet, set_size, printSearchAtLeastOneElement);
                    printf("#%d: SEARCH_AT_LEAST_ONE_ELEMENT %ld ms\n", rank, stopTime());
                    break;
                case SEARCH_NO_ELEMENTS:
                    startTime();
                    searchNoElements(array, i, searchedSet, set_size, printSearchNoElements);
                    printf("#%d: SEARCH_NO_ELEMENTS %ld ms\n", rank, stopTime());
                    break;
                case SEARCH_TRANSFER_SET:
                    MPI_Recv(&set_size, 1, MPI_INT, ROOT, TRANSFER_SET_SIZE, MPI_COMM_WORLD, &status);
                    MPI_Recv(searchedSet, set_size, MPI_INT, ROOT, TRANSFER_SET, MPI_COMM_WORLD, &status);
                    break;
            }
        } while(search_option != SEARCH_QUIT);
    }

    MPI_Finalize();
}



