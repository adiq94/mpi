#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <limits.h>
#include <sys/time.h>

#define SET_SIZE_MIN 3
#define SET_SIZE_MAX 40
#define MIN_ELEMENT_VALUE 1
#define MAX_ELEMENT_VALUE 255
#define ARRAY_SIZE 10000000
#define PRINT_ARRAY_COUNT 1


int randomInt(int min, int max){
    return min + rand() % (max+1 - min);
}

int* createArray(int n)
{
    int* values = calloc(n, sizeof(int));
    return values;
}

int** create2DArray(int m, int n)
{
    int* values = calloc(m*n, sizeof(int));
    int** rows = malloc(n*sizeof(int*));
    for (int i=0; i<n; ++i)
    {
        rows[i] = values + i*m;
    }
    return rows;
}

void destroyArray(int* arr)
{
    free(arr);
}

void destroy2DArray(int** arr)
{
    free(*arr);
    free(arr);
}

int compare(const void * a, const void * b)
{
    int _a = *(int*)a;
    int _b = *(int*)b;
    if(_a < _b) return -1;
    else if(_a == _b) return 0;
    else return 1;
}

void generateUniqueValues(int *array, int size){
    for(int i = 0; i < size; i++){
        int newValue;
        int unique;
        do{
            newValue = randomInt(MIN_ELEMENT_VALUE, MAX_ELEMENT_VALUE);
            unique = 1;
            for(int j = 0; j < i; j++){
                if(array[j] == newValue){
                    unique = 0;
                    break;
                }
            }
        } while(!unique);
        array[i] = newValue;
    }
}

int **generateArrayOfSets(){
    int **array = create2DArray(SET_SIZE_MAX, ARRAY_SIZE);
    for(int i = 0; i < ARRAY_SIZE; i++){
        int size = randomInt(SET_SIZE_MIN, SET_SIZE_MAX - 1);
        generateUniqueValues(array[i], size);
        qsort(array[i], size, sizeof(int), compare);
    }
    return array;
}

int *generateSet(int size){
    int *array = createArray(size);
    generateUniqueValues(array, size);
    qsort(array, size, sizeof(int), compare);
    return array;
}

int getSize(int *array){
    int i = 0;
    while(array[i] != 0) i++;
    return i; 
}

int getMinSize(int **array){
    int min = INT_MAX;
    for(int i = 0; i < ARRAY_SIZE; i++){
        int size = getSize(array[i]);
        if(size < min) min = size;
    }
    return min;
}

int getMaxSize(int **array){
    int max = INT_MIN;
    for(int i = 0; i < ARRAY_SIZE; i++){
        int size = getSize(array[i]);
        if(size > max) max = size;
    }
    return max;
}

void searchAllElements(int **array, int arraySize, int *searchedSet, int setSize, void (*foundCallback)(int*, int, int)){
    int foundCount = 0;
    for(int i = 0; i < arraySize; i++){
        int innerSize = getSize(array[i]);
        for(int j = 0, k = setSize; j < innerSize; j++) {
            if(array[i][j] == searchedSet[setSize - k]){
                k--;
            }
            if(k == 0){
                foundCallback(array[i], innerSize, 1);
                foundCount++;
                break;
            }
        }
    }
    if(!foundCount) foundCallback(0, 0, 0);
}

void searchAtLeastOneElement(int **array, int arraySize, int *searchedSet, int setSize, void (*foundCallback)(int*, int, int)){
    int foundCount = 0;
    for(int i = 0; i < arraySize; i++){
        int innerSize = getSize(array[i]);
        for(int j = 0; j < innerSize; j++) {
            for(int k = 0; k < setSize; k++){
                if(array[i][j] == searchedSet[k]){
                    foundCallback(array[i], innerSize, 1);
                    foundCount++;
                    break;
                }
            }
        }
    }
    if(!foundCount) foundCallback(0, 0, 0);
}

void searchNoElements(int **array, int arraySize, int *searchedSet, int setSize, void (*foundCallback)(int*, int, int) ){
    int foundCount = 0;
    for(int i = 0; i < arraySize; i++){
        int innerSize = getSize(array[i]);
        int found = 0;
        for(int j = 0; j < innerSize; j++) {
            for(int k = 0; k < setSize; k++){
                if(array[i][j] == searchedSet[k]){
                    found = 1;
                }
            }
        }
        if(!found){
            foundCallback(array[i], innerSize, 1);
            foundCount++;
            break;
        }
    }
    if(!foundCount) foundCallback(0, 0, 0);
}

int rank;
int all = 0;
int leastOne = 0;
int noElements = 0;

void printArray(int *array, int size, char *found, char *notFound, int *numberOfPrints){
    if(*numberOfPrints == 0){
        printf("#%d: %s \n", rank, notFound);
    }
    else if(*numberOfPrints <= PRINT_ARRAY_COUNT){
        printf("#%d: %s:", rank, found);
        for(int i = 0; i < size; i++){
            printf(" %d", array[i]);
        }
        printf("\n");
    }
    
}

void printSearchAllElements(int *array, int size, int found){
    if(found) all++;
    printArray(array, size, "All elements match found", "No set found, where all elements match", &all);
}

void printSearchAtLeastOneElement(int *array, int size, int found){
    if(found) leastOne++;
    printArray(array, size, "At least one element match found", "No set found, where at least one element matches", &leastOne);
}

void printSearchNoElements(int *array, int size, int found){
    if(found) noElements++;
    printArray(array, size, "No elements match found", "No set found, where no element matches", &noElements);
}

long start, end;
struct timeval timecheck;

void startTime(){
    gettimeofday(&timecheck, NULL);
    start = (long)timecheck.tv_sec * 1000 + (long)timecheck.tv_usec / 1000;
}

long stopTime(){
    gettimeofday(&timecheck, NULL);
    end = (long)timecheck.tv_sec * 1000 + (long)timecheck.tv_usec / 1000;
    return end - start;
}